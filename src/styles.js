export default (props) => {
    return {
        container: {
            borderStyle: "solid",
            borderWidth: "9px",
            borderRadius: "10px",
            borderColor: props.borderColor || "black",
            margin: "10px",
            padding: "10px",
            color: props.textColor || "#ffffff",
            backgroundColor: props.bgColor || "#009DDC",
            width: "150px"
        },
        button: {
            fontStyle: "italic",
            color: props.btnTextColor || "black",
            backgroundColor: props.btnBgColor || "white"
        },
        graphPaper: {
            border: "2px solid red",
            borderRadius: "10px",
            margin: "0"
        },
        graphDark: {
            border: "2px solid red",
            borderRadius: "10px",
            margin: "0",
            backgroundImage: "url('bg5.jpg')" //http://allfreedesigns.com/wp-content/uploads/2009/07/paper-background-6.jpg
        },

        twelveLead: {
            display: "grid",
            gridTemplateColumns: "1fr 1fr 1fr 1fr",
            gridTemplateRows: "1fr 1fr 1fr",
            gridGap: "6px"
        },
        rhythmStrips: {
            display: "grid",
            gridTemplateColumns: "1fr",
            gridTemplateRows: "1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr",
            gridGap: "10px 0"
        }
    }
}