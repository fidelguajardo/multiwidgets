import React from 'react'
import { render } from 'react-dom'
import AppState from './AppState'
import Counter from './widgets/counter'
import PatientsActive from './widgets/patients-active'
import PatientsDischarged from './widgets/patients-discharged'
import Chart1 from './widgets/chart1'
import Chart2 from './widgets/chart2'
import ChartTest from './widgets/chartTest'
import Ekg from './widgets/ekg'

const COUNTERS = document.querySelectorAll(".as-counter");
const PATIENTS_ACTIVE = document.querySelectorAll(".as-patients-active");
const PATIENTS_DISCHARGED = document.querySelectorAll(".as-patients-discharged");
const CHART1 = document.querySelectorAll(".as-chart1");
const CHART2 = document.querySelectorAll(".as-chart2");
const EKG = document.querySelectorAll(".as-ekg");
const CHARTTEST = document.querySelectorAll(".as-test");

let appState; //Common appState object but can have muliple appStates if necessary
let i;

//COUNTERS.forEach((widget, index) => { //IE does not support forEach on NodeLists
for (i = 0; i < COUNTERS.length; i++) {
  appState = new AppState(); //each widget has its own state
  render(
    <Counter appState={appState} {...COUNTERS[i].dataset} />,  //dataset supported by IE11+, may need to use element.attributes
    COUNTERS[i]
  );
};

for (i = 0; i < PATIENTS_ACTIVE.length; i++) {
  appState = new AppState(); //each widget has its own state
  render(
    <PatientsActive appState={appState} {...PATIENTS_ACTIVE[i].dataset} />,  //dataset supported by IE11+, may need to use element.attributes
    PATIENTS_ACTIVE[i]
  );
};

for (i = 0; i < PATIENTS_DISCHARGED.length; i++) {
  appState = new AppState(); //each widget has its own state
  render(
    <PatientsDischarged appState={appState} {...PATIENTS_DISCHARGED[i].dataset} />,  //dataset supported by IE11+, may need to use element.attributes
    PATIENTS_DISCHARGED[i]
  );
};

for (i = 0; i < CHART1.length; i++) {
  //appState = new AppState(); //each widget has its own state
  render(
    <Chart1 {...CHART1[i].dataset} />,  //dataset supported by IE11+, may need to use element.attributes
    CHART1[i]
  );
};

for (i = 0; i < CHART2.length; i++) {
  //appState = new AppState(); //each widget has its own state
  render(
    <Chart2 {...CHART2[i].dataset} />,  //dataset supported by IE11+, may need to use element.attributes
    CHART2[i]
  );
};

for (i = 0; i < EKG.length; i++) {
  appState = new AppState(); //each EKG has its own state, but sub-components share the same state
  render(
    <Ekg appState={appState} {...EKG[i].dataset} />,  //dataset supported by IE11+, may need to use element.attributes
    EKG[i]
  );
};

for (i = 0; i < CHARTTEST.length; i++) {
  //appState = new AppState(); //each EKG has its own state, but sub-components share the same state
  render(
    <ChartTest appState={appState} {...CHARTTEST[i].dataset} />,  //dataset supported by IE11+, may need to use element.attributes
    CHARTTEST[i]
  );
};