import React from 'react'
import { Col, CardPanel } from 'react-materialize'

const PatientDetail = (props) => {
    return (
        <Col s={12} m={6} l={4}>
            <CardPanel className="teal lighten-4 black-text valign-wrapper hoverable">
                <img src={props.patient.picture.large} alt={props.patient.name.first} />
                <div className="patientInfo">
                    <b>{props.patient.name.title}&nbsp;{props.patient.name.first}&nbsp;{props.patient.name.last}</b>
                    <br />
                    {props.patient.gender}
                    <br />
                    {props.patient.dob}
                    <br />
                    {props.patient.email}
                    <br />
                    {props.patient.phone}
                </div>
            </CardPanel>
        </Col>
    )
}

export default PatientDetail