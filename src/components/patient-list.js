import React from 'react'
import PatientDetail from './patient-detail'

const PatientList = (props) => {
    const RenderedPatients = props.patients.map((patient, index) =>
        <PatientDetail key={index} patient={patient} />
    )

    return (
        <div>
            {RenderedPatients}
        </div>
    )
}

export default PatientList