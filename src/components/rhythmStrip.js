import React, { Component } from 'react';
import { VictoryChart, VictoryAxis, VictoryLine, VictoryLabel, VictoryBrushContainer } from 'victory';
import Styles from '../styles';
import { observer } from 'mobx-react';
import { toJS } from 'mobx';

const App = observer((props) => {
    const { chartDomain } = props.appState
    const styles = Styles(props)
    const handleDomain = (domain) => {
        props.appState.setDomain(domain);
    }
    return (
        <div style={styles.graphPaper}>
            <VictoryChart
                padding={0}
                height={100}
                containerComponent={
                    <VictoryBrushContainer responsive={true}
                        dimension="x"
                        selectedDomain={{ x: toJS(chartDomain).x, y: [-10, 10] }} //fixed y brush
                        onDomainChange={handleDomain}
                        selectionStyle={{ fill: "#555", opacity: 0.2 }}
                    />
                }
            >
                <VictoryLabel
                    text={props.patientData}
                    x="3"
                    y="5"
                    style={{
                        fontWeight: "bold",
                        fontSize: "6"
                    }}
                />
                <VictoryAxis
                    tickValues={[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]}
                    style={{
                        axis: { stroke: "red" },
                        grid: {
                            stroke: (tick) => tick % 5 === 0 ? "red" : "pink",
                            strokeWidth: (tick) => tick % 5 === 0 ? "2" : "1"
                        },
                        tickLabels: { fill: "none" }
                    }}
                />
                <VictoryAxis
                    dependentAxis
                    tickValues={[-10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]}
                    style={{
                        axis: { stroke: "red" },
                        grid: {
                            stroke: (tick) => tick % 5 === 0 ? "red" : "pink",
                            strokeWidth: (tick) => tick % 5 === 0 ? "2" : "1"
                        },
                        tickLabels: { fill: "none" }
                    }}
                />
                <VictoryLine
                    interpolation={"monotoneX"}
                    style={{
                        data: {
                            stroke: "black",
                            strokeWidth: "2"
                        }
                    }}
                    data={[
                        { x: 0, y: 0 },
                        { x: 1, y: 2 },
                        { x: 2, y: -3 },
                        { x: 3, y: 5 },
                        { x: 4, y: -4 },
                        { x: 5, y: 7 },
                        { x: 6, y: -2 },
                        { x: 7, y: 10 },
                        { x: 8, y: -5 },
                        { x: 9, y: 4 },
                        { x: 11, y: -7 },
                        { x: 12, y: 5 },
                        { x: 13, y: -4 },
                        { x: 14, y: 7 },
                        { x: 15, y: 0 },
                        { x: 16, y: 2 },
                        { x: 17, y: -3 },
                        { x: 18, y: 5 },
                        { x: 19, y: -3 },
                        { x: 20, y: 5 }
                    ]}
                />
            </VictoryChart>
        </div>
    )
}
)

export default App