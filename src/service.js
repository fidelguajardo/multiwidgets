import axios from 'axios'

const service = {
    getPatientsActive: () => axios.get('https://randomuser.me/api/?results=6&nat=us&seed=active'),
    getPatientsDischarged: () => axios.get('https://randomuser.me/api/?results=10&nat=us&seed=discharged')
}

export default service