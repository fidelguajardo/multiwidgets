import { observable } from 'mobx';

class AppState {
  @observable count = 0
  @observable chartDomain = {}

  patientData = {}

  increment() {
    this.count++
  }

  decrement() {
    this.count--
  }

  setDomain(domain) {
    this.chartDomain = domain
  }
}

export default AppState
