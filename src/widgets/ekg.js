import React, { Component } from 'react';
import SingleLead from '../components/singleLead';
import RhythmStrip from '../components/rhythmStrip';
import Styles from '../styles';

class App extends Component {
    constructor(props) {
        super(props);
        this.styles = Styles(props);
    }
    componentWillMount() {
        this.props.appState.setDomain({ x: [1, 4] });
    }
    render() {
        return ( //All subcomponents share the same appState. CSS Grid requires all tags closed
            <div>
                <div style={this.styles.twelveLead}>
                    <SingleLead appState={this.props.appState} className="data-area1" patientData="I"></SingleLead>
                    <SingleLead appState={this.props.appState} className="data-area2" patientData="aVR"></SingleLead>
                    <SingleLead appState={this.props.appState} className="data-area3" patientData="V1"></SingleLead>
                    <SingleLead appState={this.props.appState} className="data-area4" patientData="V4"></SingleLead>

                    <SingleLead appState={this.props.appState} className="data-area5" patientData="II"></SingleLead>
                    <SingleLead appState={this.props.appState} className="data-area6" patientData="aVL"></SingleLead>
                    <SingleLead appState={this.props.appState} className="data-area7" patientData="V2"></SingleLead>
                    <SingleLead appState={this.props.appState} className="data-area8" patientData="V5"></SingleLead>

                    <SingleLead appState={this.props.appState} className="data-area9" patientData="III"></SingleLead>
                    <SingleLead appState={this.props.appState} className="data-area10" patientData="aVF"></SingleLead>
                    <SingleLead appState={this.props.appState} className="data-area11" patientData="V3"></SingleLead>
                    <SingleLead appState={this.props.appState} className="data-area12" patientData="V6"></SingleLead>
                </div>
                <div style={this.styles.rhythmStrips}>
                    <RhythmStrip appState={this.props.appState} className="data-strip1" patientData="I"></RhythmStrip>
                    <RhythmStrip appState={this.props.appState} className="data-strip2" patientData="aVR"></RhythmStrip>
                    <RhythmStrip appState={this.props.appState} className="data-strip3" patientData="V1"></RhythmStrip>
                    <RhythmStrip appState={this.props.appState} className="data-strip4" patientData="V4"></RhythmStrip>

                    <RhythmStrip appState={this.props.appState} className="data-strip5" patientData="II"></RhythmStrip>
                    <RhythmStrip appState={this.props.appState} className="data-strip6" patientData="aVL"></RhythmStrip>
                    <RhythmStrip appState={this.props.appState} className="data-strip7" patientData="V2"></RhythmStrip>
                    <RhythmStrip appState={this.props.appState} className="data-strip8" patientData="V5"></RhythmStrip>

                    <RhythmStrip appState={this.props.appState} className="data-strip9" patientData="III"></RhythmStrip>
                    <RhythmStrip appState={this.props.appState} className="data-strip10" patientData="aVF"></RhythmStrip>
                    <RhythmStrip appState={this.props.appState} className="data-strip11" patientData="V3"></RhythmStrip>
                    <RhythmStrip appState={this.props.appState} className="data-strip12" patientData="V6"></RhythmStrip>
                </div>
            </div>
        );
    }
}

export default App;
