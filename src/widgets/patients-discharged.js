import React, { Component } from 'react';
import PatientList from '../components/patient-list';
import { Row } from 'react-materialize'
import service from '../service'



class App extends Component {
    constructor(props) {
        super(props)
        this.state = { patients: [] }
    }

    async patients() {
        try {
            const people = await service.getPatientsDischarged()
            this.setState({ patients: people.data.results })
        } catch (err) {
            console.error(err)
            this.setState({ patients: [] })
        }
    }

    componentWillMount() {
        this.patients()
    }

    render() {
        return (
            <Row>
                <PatientList patients={this.state.patients} />
            </Row>
        )
    }
}

export default App;
