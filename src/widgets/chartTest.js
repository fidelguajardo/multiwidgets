import React, { Component } from 'react';
import { VictoryChart, VictoryAxis, VictoryLine, VictoryZoomContainer, VictoryBrushContainer } from 'victory';
import { observer } from 'mobx-react';
import { toJS } from 'mobx';

@observer
class App extends Component {

    handleDomain(domain) {
        //this.setState({ selectedDomain: domain, zoomDomain: domain });
        this.props.appState.setDomain(domain);
    }

    componentWillMount() {
        this.props.appState.setDomain({ x: [2, 4] });
    }
    render() {
        const chartStyle = { parent: { minWidth: "100%", marginLeft: "10%" } };
        const { chartDomain } = this.props.appState
        return (
            <div>

                <VictoryChart width={1000} height={400} style={chartStyle}
                    containerComponent={
                        <VictoryZoomContainer responsive={false}
                            zoomDomain={toJS(chartDomain)}
                            onDomainChange={this.handleDomain.bind(this)}
                        />
                    }
                >
                    <VictoryLine
                        style={{
                            data: { stroke: "blue" }
                        }}
                        data={[
                            { x: 1, y: 125 },
                            { x: 2, y: 257 },
                            { x: 3, y: 145 },
                            { x: 4, y: 212 },
                            { x: 5, y: 132 },
                            { x: 6, y: 305 },
                            { x: 7, y: 270 },
                            { x: 8, y: 470 }
                        ]}
                    />

                </VictoryChart>

                <VictoryChart width={1000} height={400} style={chartStyle}
                    containerComponent={
                        <VictoryZoomContainer responsive={false}
                            zoomDomain={toJS(chartDomain)}
                            onDomainChange={this.handleDomain.bind(this)}
                        />
                    }
                >
                    <VictoryLine
                        style={{
                            data: { stroke: "tomato" }
                        }}
                        data={[
                            { x: 1, y: 125 },
                            { x: 2, y: 257 },
                            { x: 3, y: 345 },
                            { x: 4, y: 515 },
                            { x: 5, y: 132 },
                            { x: 6, y: 305 },
                            { x: 7, y: 270 },
                            { x: 8, y: 470 }
                        ]}
                    />

                </VictoryChart>

                <VictoryChart
                    padding={{ top: 0, left: 50, right: 50, bottom: 30 }}
                    width={1000} height={100} style={chartStyle}
                    containerComponent={
                        <VictoryBrushContainer responsive={false}
                            dimension="x"
                            selectedDomain={{ x: toJS(chartDomain).x, y: [125, 500] }} //fixed y brush
                            onDomainChange={this.handleDomain.bind(this)}
                        />
                    }
                >
                    <VictoryAxis
                        tickValues={[
                            1,
                            2,
                            3,
                            4,
                            5,
                            6,
                            7,
                            8
                        ]}
                    //tickFormat={(x) => new Date(x).getFullYear()}
                    />
                    <VictoryLine
                        style={{
                            data: { stroke: "blue" }
                        }}
                        data={[
                            { x: 1, y: 125 },
                            { x: 2, y: 257 },
                            { x: 3, y: 145 },
                            { x: 4, y: 212 },
                            { x: 5, y: 132 },
                            { x: 6, y: 305 },
                            { x: 7, y: 270 },
                            { x: 8, y: 470 }
                        ]}
                    />
                </VictoryChart>


                <VictoryChart
                    padding={{ top: 0, left: 50, right: 50, bottom: 30 }}
                    width={1000} height={100} style={chartStyle}
                    containerComponent={
                        <VictoryBrushContainer responsive={false}
                            dimension="x"
                            selectedDomain={{ x: toJS(chartDomain).x, y: [125, 500] }} //fixed y brush
                            onDomainChange={this.handleDomain.bind(this)}
                        />
                    }
                >
                    <VictoryAxis
                        tickValues={[
                            1,
                            2,
                            3,
                            4,
                            5,
                            6,
                            7,
                            8
                        ]}
                    //tickFormat={(x) => new Date(x).getFullYear()}
                    />
                    <VictoryLine
                        style={{
                            data: { stroke: "tomato" }
                        }}
                        data={[
                            { x: 1, y: 125 },
                            { x: 2, y: 257 },
                            { x: 3, y: 345 },
                            { x: 4, y: 515 },
                            { x: 5, y: 132 },
                            { x: 6, y: 305 },
                            { x: 7, y: 270 },
                            { x: 8, y: 470 }
                        ]}
                    />
                </VictoryChart>

            </div>
        );
    }
}

export default App