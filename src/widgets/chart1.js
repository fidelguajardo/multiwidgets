import React, { Component } from 'react';
import { VictoryChart, VictoryZoomContainer, VictoryLine, VictoryAxis, VictoryBrushContainer } from 'victory';
import Theme from '../theme'

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    handleZoom(domain) {
        this.setState({ selectedDomain: domain });
        console.log(this.state.selectedDomain);
    }

    handleBrush(domain) {
        this.setState({ zoomDomain: domain });
    }

    render() {
        const chartStyle = { parent: { minWidth: "100%", marginLeft: "2%" } };
        return (
            <div>
                <VictoryChart theme={Theme} width={700} height={250} scale={{ x: "time" }} style={chartStyle}
                    containerComponent={
                        <VictoryZoomContainer responsive={true}
                            dimension="x"
                            zoomDomain={this.state.zoomDomain}
                            onDomainChange={this.handleZoom.bind(this)}
                        />
                    }
                >
                    <VictoryLine
                        style={{
                            data: { stroke: "tomato" }
                        }}
                        data={[
                            { x: new Date(1982, 1, 1), y: 125 },
                            { x: new Date(1987, 1, 1), y: 257 },
                            { x: new Date(1993, 1, 1), y: 345 },
                            { x: new Date(1997, 1, 1), y: 515 },
                            { x: new Date(2001, 1, 1), y: 132 },
                            { x: new Date(2005, 1, 1), y: 305 },
                            { x: new Date(2011, 1, 1), y: 270 },
                            { x: new Date(2015, 1, 1), y: 470 }
                        ]}
                    />

                </VictoryChart>

                <VictoryChart
                    padding={{ top: 0, left: 50, right: 50, bottom: 30 }}
                    width={700} height={100} scale={{ x: "time" }} style={chartStyle}
                    containerComponent={
                        <VictoryBrushContainer responsive={true}
                            dimension="x"
                            selectedDomain={this.state.selectedDomain}
                            onDomainChange={this.handleBrush.bind(this)}
                            selectionStyle={{ fill: "teal", opacity: 0.2 }}
                        />
                    }
                >
                    <VictoryAxis
                        tickValues={[
                            new Date(1985, 1, 1),
                            new Date(1990, 1, 1),
                            new Date(1995, 1, 1),
                            new Date(2000, 1, 1),
                            new Date(2005, 1, 1),
                            new Date(2010, 1, 1)
                        ]}
                        tickFormat={(x) => new Date(x).getFullYear()}
                    />
                    <VictoryLine
                        style={{
                            data: { stroke: "tomato" }
                        }}
                        data={[
                            { x: new Date(1982, 1, 1), y: 125 },
                            { x: new Date(1987, 1, 1), y: 257 },
                            { x: new Date(1993, 1, 1), y: 345 },
                            { x: new Date(1997, 1, 1), y: 515 },
                            { x: new Date(2001, 1, 1), y: 132 },
                            { x: new Date(2005, 1, 1), y: 305 },
                            { x: new Date(2011, 1, 1), y: 270 },
                            { x: new Date(2015, 1, 1), y: 470 }
                        ]}
                    />
                </VictoryChart>

            </div>
        );
    }
}

export default App;